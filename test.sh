#!/bin/bash

min_nodes=10000
min_edges=1

step=$min_nodes
edge_step=$step*100

max_nodes=100002

nodes=$min_nodes
max_edges=$(($min_nodes*($min_nodes-1)/2))


while [ $nodes -le $max_nodes ]; do
    edges=1
    echo "$nodes/$max_nodes"
    while [ $edges -le $max_edges ]; do
        ./gis_generator $nodes $edges > tmp.tgf
        seconds=$((/usr/bin/time -f %U ./gis_solver tmp.tgf /dev/null) 2>&1)
        echo "$nodes $edges $seconds" >> time_test.data
        let edges=$edges+$edge_step
    done
    echo "" >> time_test.data
    let nodes=$nodes+$step
done
