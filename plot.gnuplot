set ticslevel 0
set xtics 20000
set ytics 10000000
set xlabel "Liczba wierzcholkow"
set ylabel "Liczba krawedzi"
set zlabel "Czas wykonania [s]      "
set format xy "%.0t*10^%T"
set pm3d
set view 60,290
set term x11
splot "time_test.data" with pm3d notitle
