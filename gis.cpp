// Wierzchołki rozdzielające grafu nieskierowanego - projekt GIS
// Autorzy: Jakub Łyskawka, Wojciech Matuszewski

#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <cstring>
#include <stdexcept>

using namespace std;

// Struktura reprezentująca wierzchołek grafu
struct Vertex {
    int dfsOrder;
    int lowValue;
    int componentId;
    string label;
    bool articulationPoint;
    vector<int> edges;
    
    Vertex() : dfsOrder(0), lowValue(0), componentId(0), articulationPoint(false) {}
};

// Graf to wektor wierzchołków
typedef vector<Vertex> Graph;

// Dodaje wierzchołek o danym identyfikatorze i etykiecie do grafu
void addNode(Graph& graph, int id, string label);

// Dodaje do grafu nieskierowaną krawędź łączącą 2 wierzchołki
void addEdge(Graph& graph, int from, int to);

// Operator wejściowy, wczytujący graf w formacie TGF
istream& operator>>(istream& is, Graph& graph);

// Operator wyjściowy, wypisujący graf w formacie TGF
ostream& operator<<(ostream& os, Graph& graph);

// Pomocnicza funkcja wypisująca wszystkie parametry wierzchołków
void dumpGraph(Graph& graph);

// Funkcja wyszukująca i oznaczająca wierzchołki rozdzielające grafu
void findArticulationPoints(Graph& graph);

// Rekurencyjna funkcja wyszukująca wierzchołki rozdzielające grafu, wyłowywana dla każdej składowej
void findArticulationPointsDFS(Graph& graph, int& order, Vertex& current, Vertex& parent);

// Funkcja wyszukująca w grafie składowe spójne, po usunięciu wierzchołków rozdzielających
void findComponents(Graph& graph);

// Funkcja odnajdująca wierzchołki należące do składowej spójnej
void findComponentDFS(Graph &graph, int componentId, Vertex& current);

int main( int argc, const char* argv[] )
{
    Graph graph;
    istream* isp;
    ostream* osp;

    for(int i=1; i<argc; ++i)
    {
        if(!strcmp(argv[i], "-h") || !strcmp(argv[i], "--help"))
        {
            cout << "Usage:" << " " << argv[0]
                 << " [input_graph [output_graph]]" << endl;
            return 0;
        }
    }

    if(argc > 1)
    {
        isp = new ifstream(argv[1]);
        if(!isp->good())
        {
            cerr << "*** Error opening " << argv[1] << " for reading." << endl;
            return 1;
        }
    }
    else
    {
        isp = &cin;
    }

    if(argc > 2)
    {
        osp = new ofstream(argv[2]);
        if(!osp->good())
        {
            cerr << "*** Error opening " << argv[2] << " for reading." << endl;
            return 1;
        }
    }
    else
    {
        osp = &cout;
    }

    istream& input = *isp;
    ostream& output = *osp;

    input >> graph;
    findArticulationPoints(graph);
    findComponents(graph);
    //dumpGraph(graph);
    output << graph;
    
    return 0;
}

void dumpGraph(Graph& graph) {
    
    for (int i = 1; i < graph.size(); ++i) {
        Vertex v = graph[i];
        cout << i << " [" << v.label << "]: dfs: " << v.dfsOrder << " low: " << v.lowValue
             << " cmp: " << v.componentId << " art: " << v.articulationPoint << " edges: { ";
        
        for (int target : v.edges) {
            cout << target << " ";
        }
        
        cout << "}\n";
    }
}

void findArticulationPoints(Graph& graph) {
    
    for (int i = 1; i < graph.size(); ++i) {
        Vertex& v = graph[i];
        
        if (v.dfsOrder == 0) { // unvisited vertex
            int order = 1;
            findArticulationPointsDFS(graph, order, v, v);
        }
    }
}

void findArticulationPointsDFS(Graph& graph, int& order, Vertex& current, Vertex& parent) {
    current.dfsOrder = order++;
    
    int neighboursVisited = 0;
    int tmpLow = current.dfsOrder;
    
    for (int i : current.edges) {
        Vertex& neighbour = graph[i];
        
        if (neighbour.dfsOrder == 0) { // unvisited neighbour
            ++neighboursVisited;
            findArticulationPointsDFS(graph, order, neighbour, current);
            
            if (current.dfsOrder != 1 && neighbour.lowValue >= current.dfsOrder) { // second condition met
                current.articulationPoint = true;
            }
            
            if (neighbour.lowValue < tmpLow) {
                tmpLow = neighbour.lowValue; // update low
            }
        }
        else { // visited neighbour
            if (&neighbour != &parent && neighbour.dfsOrder < tmpLow) {
                tmpLow = neighbour.dfsOrder; // update low
            }
        }
    }
    
    current.lowValue = tmpLow;
    
    if (current.dfsOrder == 1 && neighboursVisited > 1) { // first condition met
        current.articulationPoint = true;
    }
}

void findComponents(Graph& graph) {
    int componentId = 1;
    
    for (int i = 1; i < graph.size(); ++i) {
        Vertex& v = graph[i];
        
        if (v.componentId == 0 && !v.articulationPoint) {
            findComponentDFS(graph, componentId, v);
            ++componentId;
        }
    }
}

void findComponentDFS(Graph &graph, int componentId, Vertex& current) {
    current.componentId = componentId;
    
    for (int i : current.edges) {
        Vertex& neighbour = graph[i];
        
        if (neighbour.componentId == 0 && !neighbour.articulationPoint) {
            findComponentDFS(graph, componentId, neighbour);
        }
    }
}

void addNode(Graph& graph, int id, string label)
{
    try
    { // in case the vertices are not sorted by id
        graph.at(id).label = label;
    }
    catch (const std::out_of_range& oor)
    { // no such vertex yet, let's resize
        graph.resize(id+1);
        graph[id].label = label;
    }
}

void addEdge(Graph& graph, int from, int to)
{  
    try
    {
        Vertex& fromVertex = graph.at(from);
        Vertex& toVertex = graph.at(to);
        
        for(int e : fromVertex.edges) // check for duplicate edges
        {
            if(e == to)
            {
                return;
            }
        }
        
        fromVertex.edges.push_back(to);
        toVertex.edges.push_back(from);
    }
    catch (const std::out_of_range& oor)
    {
        cerr << "*** Invalid vertex" << endl;
    }
}

istream& operator>>(istream& is, Graph& graph)
{
    while(is.good() && is.peek() != '#')
    {
        int id;
        string label;
        is >> id;
        
        if(!is.good()) // something might go wrong
            break;
        
        if(is.get() != '\n')
            getline(is, label);
        
        addNode(graph, id, label);
    }
    
    while(is.good() && is.get() != '\n');
    
    while(is.good())
    {
        int from, to;
        string label;
        
        is >> from >> to;
        
        if(!is.good()) // something might go wrong
            break;
        
        if(is.get() != '\n')
        {
            string label;
            getline(is, label);
        }
        
        addEdge(graph, from, to);
    }
    
    return is;
}

ostream& operator<<(ostream& os, Graph& graph)
{
    for (int i = 1; i < graph.size(); ++i) {
        Vertex& v = graph[i];
        
        os << i << " " << v.label;

        if (v.articulationPoint) {
            os << " [Articulation Point]";
        }
        else {
            os << " [Component " << v.componentId << "]";
        }

        os << endl;
    }
    
    os << "#" << endl;

    for(int i=0; i<graph.size(); ++i)
    {
        for(int to : graph[i].edges)
        {
            os << i << " " << to << endl;

            vector<int>& toVertexEdges = graph[to].edges;

            for(vector<int>::iterator it=toVertexEdges.begin(); it!=toVertexEdges.end(); ++it)
            {
                if(*it == i)
                {
                    toVertexEdges.erase(it);
                    break;
                }    
            }
        }
    }
    
    return os;
}
