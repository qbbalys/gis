#include <iostream>
#include <cstdlib>
#include <ctime>

const int minNodes = 2;
const int maxNodes = 20;
const int minEdges = 1;
const int maxEdges = 50;

int main(int argc, char **argv)
{
    srand(time(NULL));
    
    int nodes = minNodes + (rand() % (maxNodes - minNodes)) + 1;
    int edges = minEdges + (rand() % (maxEdges - minEdges)) + 1;

    if(argc >= 2) {
        nodes = atoi(argv[1]);
    }

    if(argc >= 3) {
        edges = atoi(argv[2]);
    }

    for(int i=1; i<=nodes; ++i) {
        std::cout << i << ' ' << i << std::endl;
    }

    std::cout << '#' << std::endl;

    for(int i=1; i<=nodes; ++i) {
        int from = (rand() % nodes) + 1;
        int to;

        do {
            to = (rand() % nodes) + 1;
        } while (from == to);
        
        std::cout << from << ' ' << to << std::endl;
    }
    
	return 0;
}

